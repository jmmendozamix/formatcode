<?php

namespace Aodamuz\FormatCode\Facades;

use Illuminate\Support\Facades\Facade;

class FormatCode extends Facade {
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() {
		return 'format';
	}
}
