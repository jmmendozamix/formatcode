<?php

namespace Aodamuz\FormatCode;

use Symfony\Component\Finder\Finder;

class FormatCode {
	/**
	 * Finder instance.
	 *
	 * @var \Symfony\Component\Finder\Finder
	 */
	protected $finder;

	/**
	 * Create a new FormatCode instance.
	 *
	 * @param \Symfony\Component\Finder\Finder $finder
	 *
	 * @return void
	 */
	public function __construct(Finder $finder) {
		$this->finder = $finder;
	}

	/**
	 * @param string $path
	 *
	 * @return void
	 */
	public function read($path) {
		if (!is_dir($path))
			return;

		foreach ($this->finder->in($path)->files()->name('*.php') as $file) {
			$code = file_get_contents($file->getRealPath());

			if (preg_match('/\nnamespace (.*?);/', $code)) {
				$code = $this->formatCode($code);
				$code = $this->sortUsages($code);
			}

			file_put_contents($file->getRealPath(), $code);
		}
	}

	/**
	 * @param string $file
	 *
	 * @return string
	 */
	protected function formatCode($file) {
		$file = str_replace('    ', "\t", $file);
		$file = str_replace("\n{\n", " {\n", $file);
		$file = str_replace("\n\t{\n", " {\n", $file);

		return $file;
	}

	/**
	 * @param $code
	 *
	 * @return array
	 */
	protected function sortUsages($code) {
		if (preg_match_all('/\nuse (.*?);/', $code, $matches)) {
			$uses = $matches[1];

			usort($uses, function($a, $b) {
				return strlen($a) - strlen($b);
			});

			foreach ($uses as $key => $value) {
				$uses[$key] = "use {$value};";
			}

			$code = preg_replace('/\nuse (.*?);/', '', $code);
			$code = str_replace(";\n\n\n", "\n\n" . implode("\n", $uses) . "\n\n", $code);
		}

		return $code;
	}
}
