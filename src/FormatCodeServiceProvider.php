<?php

namespace Aodamuz\FormatCode;

use Illuminate\Support\ServiceProvider;

class FormatCodeServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any package services.
	 *
	 * @return void
	 */
	public function boot() {
		//
	}

	/**
	 * Register any package services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton('format', function ($app) {
			return $app->make('Aodamuz\FormatCode\FormatCode');
		});
	}
}
